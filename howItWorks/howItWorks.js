/**
 * Created by klos on 07.03.2016.
 */
(function() {
    var firstCircle = document.getElementById("firstCircle");
    var secondCircle = document.getElementById("secondCircle");
    var thirdCircle = document.getElementById("thirdCircle");
    var fourthCircle = document.getElementById("fourthCircle");
    var firstArrow = document.getElementById("firstArrow");
    var secondArrow = document.getElementById("secondArrow");
    var thirdArrow = document.getElementById("thirdArrow");
    firstCircle.onmouseover = function() {
        firstArrow.style.visibility='hidden';
    };
    firstCircle.onmouseout = function() {
        firstArrow.style.visibility='visible';
    };
    secondCircle.onmouseover = function() {
        firstArrow.style.visibility='hidden';
        secondArrow.style.visibility='hidden';
    };
    secondCircle.onmouseout = function() {
        firstArrow.style.visibility='visible';
        secondArrow.style.visibility='visible';
    };
    thirdCircle.onmouseover = function() {
        secondArrow.style.visibility='hidden';
        thirdArrow.style.visibility='hidden';
    };
    thirdCircle.onmouseout = function() {
        secondArrow.style.visibility='visible';
        thirdArrow.style.visibility='visible';
    };
    fourthCircle.onmouseover = function() {
        thirdArrow.style.visibility='hidden';
    };
    fourthCircle.onmouseout = function showThird() {
        thirdArrow.style.visibility='visible';
    };
})();