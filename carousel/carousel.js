/**
 * Created by klos on 07.03.2016.
 */
    document.getElementById('firstCircle').onclick = function firstText() {
        document.getElementById('secondText').innerHTML = "";
        document.getElementById('thirdText').innerHTML = "";
        document.getElementById('fourthText').innerHTML = "";
        document.getElementById('firstText').innerHTML = "Założenie konta w naszym" + '<br>' +
        'serwisie zajmie Ci mniej' + '<br>' +
        'niż minutę z Twojego życia.' + '<br>' +
        'Zaryzykuj i przekonaj się' + '<br>' +
        'o niesamowitych możliwościach' + '<br>' +
        'naszej aplikacji';

        document.getElementById('firstDot').className = "active";
        document.getElementById('secondDot').className = "";
        document.getElementById('thirdDot').className = "";
        document.getElementById('fourthDot').className = "";
    };

    document.getElementById('secondCircle').onclick = function secondText() {
        document.getElementById('firstText').innerHTML = "";
        document.getElementById('thirdText').innerHTML = "";
        document.getElementById('fourthText').innerHTML = "";
        document.getElementById('secondText').innerHTML = "Rób zdjęcia jednym kliknięciem" + '<br>' +
        'wszędzie gdzie jesteś.' + '<br>' +
        'Robienie zdjęć jeszcze' + '<br>' +
        'nigdy nie było tak proste.' + '<br>' +
        'Pamiętaj, że 1 obraz mówi' + '<br>' +
        'więcej niż 1000 słów';

        document.getElementById('firstDot').className = "";
        document.getElementById('secondDot').className = "active";
        document.getElementById('thirdDot').className = "";
        document.getElementById('fourthDot').className = "";
    };

    document.getElementById('thirdCircle').onclick = function thirdText() {
        document.getElementById('firstText').innerHTML = "";
        document.getElementById('secondText').innerHTML = "";
        document.getElementById('fourthText').innerHTML = "";
        document.getElementById('thirdText').innerHTML = "Nie potrzebujesz już" + '<br>' +
        'więcej skomplikowanego' + '<br>' +
        'oprogramowania do obróbki' + '<br>' +
        'zdjęć. Skorzystaj z olbrzymiej' + '<br>' +
        'bazy filtrów, które masz od' + '<br>' +
        'nas całkowicie za darmo';

        document.getElementById('firstDot').className = "";
        document.getElementById('secondDot').className = "";
        document.getElementById('thirdDot').className = "active";
        document.getElementById('fourthDot').className = "";
    };

    document.getElementById('fourthCircle').onclick = function fourthText() {
        document.getElementById('firstText').innerHTML = "";
        document.getElementById('secondText').innerHTML = "";
        document.getElementById('thirdText').innerHTML = "";
        document.getElementById('fourthText').innerHTML = "Voilà!" + '<br>' +
        'Wszystko jest gotowe!' + '<br>' +
        'Oglądaj u publikuj swoje' + '<br>' +
        'zdjęcia w naszej aplikacji.' + '<br>' +
        'Cieszymy się, że jesteś' + '<br>' +
        'z nami na pokładzie!';

        document.getElementById('firstDot').className = "";
        document.getElementById('secondDot').className = "";
        document.getElementById('thirdDot').className = "";
        document.getElementById('fourthDot').className = "active";
    };


    document.getElementById('firstDot').onclick = function firstDotClick() {
        document.getElementById('secondText').innerHTML = "";
        document.getElementById('thirdText').innerHTML = "";
        document.getElementById('fourthText').innerHTML = "";

        document.getElementById('firstDot').className = "active";
        document.getElementById('secondDot').className = "";
        document.getElementById('thirdDot').className = "";
        document.getElementById('fourthDot').className = "";

        document.getElementById('firstText').innerHTML = "Założenie konta w naszym" + '<br>' +
        'serwisie zajmie Ci mniej' + '<br>' +
        'niż minutę z Twojego życia.' + '<br>' +
        'Zaryzykuj i przekonaj się' + '<br>' +
        'o niesamowitych możliwościach' + '<br>' +
        'naszej aplikacji';
    };

    document.getElementById('secondDot').onclick = function secondDotClick() {
        document.getElementById('firstText').innerHTML = "";
        document.getElementById('thirdText').innerHTML = "";
        document.getElementById('fourthText').innerHTML = "";

        document.getElementById('firstDot').className = "";
        document.getElementById('secondDot').className = "active";
        document.getElementById('thirdDot').className = "";
        document.getElementById('fourthDot').className = "";

        document.getElementById('secondText').innerHTML = "Rób zdjęcia jednym kliknięciem" + '<br>' +
        'wszędzie gdzie jesteś.' + '<br>' +
        'Robienie zdjęć jeszcze' + '<br>' +
        'nigdy nie było tak proste.' + '<br>' +
        'Pamiętaj, że 1 obraz mówi' + '<br>' +
        'więcej niż 1000 słów';
    };

    document.getElementById('thirdDot').onclick = function thirdDotClick() {
        document.getElementById('firstText').innerHTML = "";
        document.getElementById('secondText').innerHTML = "";
        document.getElementById('fourthText').innerHTML = "";

        document.getElementById('firstDot').className = "";
        document.getElementById('secondDot').className = "";
        document.getElementById('thirdDot').className = "active";
        document.getElementById('fourthDot').className = "";

        document.getElementById('thirdText').innerHTML = "Nie potrzebujesz już" + '<br>' +
        'więcej skomplikowanego' + '<br>' +
        'oprogramowania do obróbki' + '<br>' +
        'zdjęć. Skorzystaj z olbrzymiej' + '<br>' +
        'bazy filtrów, które masz od' + '<br>' +
        'nas całkowicie za darmo';
    };

    document.getElementById('fourthDot').onclick = function fourthDotClick() {
        document.getElementById('firstText').innerHTML = "";
        document.getElementById('secondText').innerHTML = "";
        document.getElementById('thirdText').innerHTML = "";

        document.getElementById('firstDot').className = "";
        document.getElementById('secondDot').className = "";
        document.getElementById('thirdDot').className = "";
        document.getElementById('fourthDot').className = 'active';

        document.getElementById('fourthText').innerHTML = "Voilà!" + '<br>' +
        'Wszystko jest gotowe!' + '<br>' +
        'Oglądaj u publikuj swoje' + '<br>' +
        'zdjęcia w naszej aplikacji.' + '<br>' +
        'Cieszymy się, że jesteś' + '<br>' +
        'z nami na pokładzie!';
    };