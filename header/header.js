/**
 * Created by klos on 07.03.2016.
 */
var deadline = 'December 31 2022 23:59:59 GMT+01:00';

function getTimeRemaining(endtime){
    var t = Date.parse(endtime) - Date.parse(new Date());
    var seconds = Math.floor( (t/1000) % 60 );
    var minutes = Math.floor( (t/1000/60) % 60 );
    var hours = Math.floor( (t/(1000*60*60)) % 24 );
    var days = Math.floor( t/(1000*60*60*24) );
    return {
        'total': t,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
    };
}

function displayHours(id, endtime){
    var clock = document.getElementById(id);
    var hoursSpan = clock.querySelector('.hours');
    function updateClock(){
        var t = getTimeRemaining(endtime);
        clock.innerHTML =
            '<p>' + ('0' + t.hours).slice(-2);
        if(t.total<=0){
            clearInterval(timeinterval);
        }
    }
    updateClock();
    var timeinterval = setInterval(updateClock,1000);
}

function displayMinutes(id, endtime){
    var clock = document.getElementById(id);
    function updateClock(){
        var t = getTimeRemaining(endtime);
        clock.innerHTML =
            '<p>' + ('0' + t.minutes).slice(-2);
        if(t.total<=0){
            clearInterval(timeinterval);
        }
    }
    updateClock();
    var timeinterval = setInterval(updateClock,1000);
}

function displaySeconds(id, endtime){
    var clock = document.getElementById(id);
    function updateClock(){
        var t = getTimeRemaining(endtime);
        clock.innerHTML =
            '<p>' + ('0' + t.seconds).slice(-2);
        if(t.total<=0){
            clearInterval(timeinterval);
        }
    }
    updateClock();
    var timeinterval = setInterval(updateClock,1000);
}

displayHours('firstCounter', deadline);
displayMinutes('secondCounter', deadline);
displaySeconds('thirdCounter', deadline);